// Query Operators

// [SECTION] Comparison Query Operators
// $gt and $gte operator (Greater than/Greater than or equal to)


// find users with an age greater than 50
db.users.find({
	age: {
		$gt: 50
	}
})

// find users with an age greater than or equal to 50
db.users.find({
	age: {
		$gte: 50
	}
})

// find users with an age less than 50
db.users.find({
	age: {
		$lt: 50
	}
})

// find users with an age less than or equal to 50
db.users.find({
	age: {
		$lte: 50
	}
})

// find users with an age that is NOT equal to 82
db.users.find({
	age: {
		$ne: 82
	}
})

// find users whose last names are either "Hawking" or "Doe"
db.users.find({
	lastName: {
		$in: ["Hawking", "Doe"]
	}
})

// find users whose courses including "HTML" OR "React"
db.users.find({
	courses: {
		$in: ["HTML", "React"]
	}
})


// [SECTION] Logical Query Operators

// $or operator

db.users.find({
	$or: [
		{
			firstName: "Neil"
		},
		{
			age: 21
		}
	]
})


// $and operator
db.users.find({
	$and: [{
		age: {
			$ne: 82
		}
	},
		{age: {
			$ne: 76
		}
	}
	]
})

// [SECTION] Inclusion

- Allows us to include/add specific fields only when retrieving 





db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1
})

// [SECTION] Exclusion
- Allows us to exclude/remove specific fields only when retrieving documents.
- The value provided is 0 to denote that the field is being excluded

db.users.find(
{
	firstName: "Jane"
},
{
	contact: 0,
	department: 0
})


// Suppressing the ID field
- Allows us to exclude the "_id" field when retrieving documents.
- When using 

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	})

// Returning Specific field in 


db.users.find(
{
	firstName: "Jane"
},
{
	"contact.phone": 0
})


db.users.find(
{ "namearr": 
	{
		namea: "juan"
	}
},
	{ namearr: 
		{$slice: 1}
	}
)


// $regex operator
/*
	- Allows us to find documents that match a specific string pattern using regular expressions.

	- Syntax
		db.users.find({field: {$regex: 'pattern'}, $options: '$optionValue'});

*/

// Case sensitive query
db.users.find({firstName: {$regex: 'N'}}).pretty();

// Case insensitive quesry
db.users.find({firstName: {$regex: 'j', $options: '$i'}}).pretty();


db.users.find({
	$or: [
			{ firstName: {$regex: 's', $options: '$i'}},
			{ lastName: { $regex: 'd', $options: '$i'}}
		]
}, {firstName: 1, lastName: 1, _id: 0 });